# Algoritma Struktur Data

Kumpulan kode yang digunakan untuk praktikum Algoritma dan Struktur Data Politeknik Negeri Malang

### Dosen pengampu mata kuliah
1. Mungki Astiningrum, ST., M.Kom.
2. Imam Fahrur Rozi, ST., M.T.
3. Mustika Mentari, S.Kom., M.Kom
4. Mamluatul Hani'ah, S.Kom., M.Kom.
5. Rokhimatul Wakhidah, S.Pd., M.T.
6. Noprianto SKom., MEng.
7. Septian Enggar Sukmana, S.Pd., M.T
