/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1;

/**
 *
 * @author Mamluatul Hani'ah
 */
public class Array1 {

    public static void main(String[] args) {
        int stockBunga[][] = {
            {10, 5, 15, 7},
            {6, 11, 9, 12},
            {2, 10, 10, 5},
            {5, 7, 12, 9}};
        String[] namaBunga = {"Aglonema", "Keladi", "Alocasia", "Mawar"};
        String[] namaCabang = {"RoyalGarden1", "RoyalGarden2", "RoyalGarden3", "RoyalGarden4"};
        
        int total = 0;
        System.out.println("stock berdasarkan jenis diseluruh cabang");
        for (int i = 0; i < stockBunga.length; i++) {
            total = 0;
            for (int j = 0; j < stockBunga[0].length; j++) {
                total = total + stockBunga[j][i];
            }
            System.out.println("bunga " + " " + namaBunga[i] + " : " + total);
        }
        System.out.println("========================================================");
        
        int[] kurang = {1, 2, 0, 5};
        long pendapatan = 0;
        int[] harga = {75000, 50000, 60000, 10000};
        System.out.println("Update stock Royal Garden 1");
        for (int i = 1; i < 2; i++) {
            int totalcab = 0;
            for (int j = 0; j < stockBunga[0].length; j++) {
                stockBunga[0][j] = stockBunga[0][j]-kurang[j];
                System.out.println(namaBunga[j]+" "+stockBunga[0][j]);
                pendapatan += (stockBunga[0][j] * harga[j]);
            }         
                      
        }
        System.out.println("pendapatan cabang RoyalGarden1 setelah pengurangan stock : " + "RP. " + pendapatan);
        System.out.println("========================================================");
        
    }
}
