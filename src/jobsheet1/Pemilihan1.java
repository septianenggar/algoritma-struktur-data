/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1;

import java.util.Scanner;

/**
 *
 * @author Mamluatul Hani'ah
 */
public class Pemilihan1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Program Menghitung Nilai Akhir");
        System.out.println("======================");
        System.out.print("Masukkan Nilai Tugas: ");
        int tugas = input.nextInt();
        System.out.print("Masukkan Nilai UTS: ");
        int UTS = input.nextInt();
        System.out.print("Masukkan Nilai UAS: ");
        int UAS = input.nextInt();
        float nilaiAkhir;
        String nilaiHuruf = null;
        System.out.println("======================");
        System.out.println("======================");
        nilaiAkhir = (float) ((0.2 * tugas) + (0.35 * UTS) + (0.45 * UAS));
        System.out.println("nilai akhir : " + nilaiAkhir);
        if (nilaiAkhir > 80 && nilaiAkhir <= 100) {
            nilaiHuruf = "A";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        } else if (nilaiAkhir > 73 && nilaiAkhir <= 80) {
            nilaiHuruf = "B+";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        } else if (nilaiAkhir > 65 && nilaiAkhir <= 73) {
            nilaiHuruf = "B";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        } else if (nilaiAkhir > 60 && nilaiAkhir <= 65) {
            nilaiHuruf = "C+";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        } else if (nilaiAkhir > 50 && nilaiAkhir <= 60) {
            nilaiHuruf = "C";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        } else if (nilaiAkhir > 39 && nilaiAkhir <= 50) {
            nilaiHuruf = "D";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        } else if (nilaiAkhir < 39) {
            nilaiHuruf = "E";
            System.out.println("Nilai Huruf :" + nilaiHuruf);
        }
        System.out.println("======================");
        if (nilaiHuruf.equals("D") || nilaiHuruf.equals("E")) {
            System.out.println("ANDA TIDAK LULUS");
        } else {
            System.out.println("SELAMAT LULUS");
        }

    }

}
