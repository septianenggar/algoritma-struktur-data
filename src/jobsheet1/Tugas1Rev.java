/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1;

import java.util.Scanner;

/**
 *
 * @author Mamluatul Hani'ah
 */
public class Tugas1Rev {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Smile Laundry ");

        int harga = 4500;
        int jumCust = 4;
        int berat;
        float disc;
        float sum = 0;
        float total = 0;
        for (int i = 0; i < jumCust; i++) {
            System.out.println("Customer ke :" + (i + 1));
            System.out.print("Masukkan berat pakaian (Kg):");
            berat = input.nextInt();
            if (berat > 10) {
                disc = (float) ((harga * berat) * 0.05);
                total = (harga * berat) - disc;
                System.out.println("Total yang harus dibayar customer ke" + (i + 1) + ":" + total);
            } else if (berat > 0 && berat <= 10) {
                total = harga * berat;
                System.out.println("Total yang harus dibayar customer ke" + (i + 1) + ":" + total);
            }
            sum = sum + total;
        }
        System.out.println("====================");
        System.out.println("total pendapatan hari ini :" + sum);

    }
}
