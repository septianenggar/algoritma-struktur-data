/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1;

import java.util.Scanner;

/**
 *
 * @author Mamluatul Hani'ah
 */
public class Tugas4 {

    static Scanner input = new Scanner(System.in);
    static Scanner input2 = new Scanner(System.in);

    static float lingkaran(float jari) {
        float luas = 3.14f * jari * jari;
        return luas;
    }

    static int segiempat(int sisi) {
        int luas = sisi * sisi;
        return luas;
    }

    static float segitiga(int alas, int tinggi) {
        float luas = alas * tinggi / 2;
        return luas;
    }

    static int menu() {
        int menu;
        System.out.println("Mengitung Luas");
        System.out.println("1. luas segitiga");
        System.out.println("2. luas segiempat");
        System.out.println("3. luas lingkaran");
        System.out.print("masukan pilihan (1-3) : ");
        menu = input.nextInt();
        return menu;

    }

    public static void main(String[] args) {
        String jawab;
        do{
        int pilMenu = menu();
        float luas = 0;
        if (pilMenu == 1) {
            int alas, tinggi;
            System.out.print("Masukan alas : ");
            alas = input.nextInt();
            System.out.print("Masukan tinggi : ");
            tinggi = input.nextInt();
            luas = segitiga(alas,tinggi);
            System.out.println("Luas segitiga adalah " + luas);
        } else if (pilMenu == 2) {
            int sisi;
            System.out.print("Masukan sisi : ");
            sisi = input.nextInt();
            luas = segiempat(sisi);
            System.out.println("Luas segiempat adalah " + luas);
        } else if (pilMenu == 3) {
            float jari;
            System.out.print("Masukan jari jari : ");
            jari = input.nextFloat();
            luas = lingkaran(jari);
            System.out.println("Luas lingkaran adalah " + luas);
        } else {
            System.out.println("inputan anda salah");
            
        }
        
        System.out.println("Apakah ingin kembali ke menu? (y/t)");
        jawab = input2.nextLine();
       
        }while(jawab.equalsIgnoreCase("y"));
        
    }

}
